INSERT INTO feed (id, title, updated, nextRun, feedType, link, cleanbuffer, tags, scrape)
VALUES(
    'b7c32ff5-b2dc-4228-8585-c6f37071338c',
    'Basic Apple Guy',
    '2022-01-01 00:00:00+00:00',
    '2022-01-01 00:00:00+00:00',
    1, -- 1=RSS, 2=ATOM, 3=RDF
    'https://basicappleguy.com/basicappleblog?format=rss',
    1, -- 1=Clean up old data
    '#blog #apple',
    0 -- Scrapecontent instead of using content in feed item
);
