'use strict';

Date.prototype.addDays = function (days) {
	const date = new Date(this.valueOf());
	date.setDate(date.getDate() + days);
	return date;
};

const prefersColorScheme = '(prefers-color-scheme: dark)';

const getUserId = () => {
	const uId = localStorage.getItem('token');

	if (!uId) {
		return getCookie('token');
	}

	return uId;
};

const getFeedId = () => {
	const divContent = document.querySelector('div.content');
	return divContent.id;
};

const addDeleteListener = () => {
	const deleteElm = document.querySelector('a.delete-feed');
	if (deleteElm) {
		deleteElm.addEventListener('click', deleteFeed, false);
	}
};

const addTableClass = () => {
	const tables = document.querySelectorAll('table');
	if (tables) {
		for (const t of tables) {
			t.classList.add('table');
			t.classList.add('table-sm');
		}
	}
};

async function getVisisted() {
	const uId = getUserId();

	if (uId) {
		const response = await fetch(`http://pihole.lan:54321/api/feeds/read/${uId}`, {
			method: 'GET',
			mode: 'cors',
			headers: new Headers({
				Accept: 'application/json',
				'Content-Type': 'application/json',
			}),
		});

		return response.json();
	}

	return [];
}

async function markAsRead() {
	const key = 'visited-' + window.location.pathname + window.location.search;
	const feedId = getFeedId();
	const uId = getUserId();

	if (feedId && uId) {
		const data = {
			visited: key,
			feed_id: feedId,
		};

		const response = await fetch(`http://pihole.lan:54321/api/feeds/read/${uId}`, {
			method: 'POST',
			mode: 'cors',
			headers: new Headers({
				Accept: 'application/json',
				'Content-Type': 'application/json',
			}),
			body: JSON.stringify(data),
		});

		return response.json();
	}
}

async function deleteFeed(event) {
	const feedId = getFeedId();

	const message = 'Are you sure you want to delete this feed?';

	if (feedId && confirm(message)) {
		const response = await fetch(`http://pihole.lan:54321/api/feeds/${feedId}`, {
			method: 'DELETE',
			mode: 'cors',
			headers: new Headers({
				Accept: 'application/json',
				'Content-Type': 'application/json',
			}),
		});

		alert('Feed was deleted');
	}

	event.preventDefault();
}

function setCookie(name, value, days) {
	let expires = '';
	if (days) {
		const date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = '; expires=' + date.toUTCString();
	}

	document.cookie = name + '=' + (value || '') + expires + '; path=/';
}

const getCookie = name => {
	const nameEQ = name + '=';
	const ca = document.cookie.split(';');
	for (let c of ca) {
		while (c.charAt(0) == ' ') {
			c = c.substring(1, c.length);
		}

		if (c.indexOf(nameEQ) == 0) {
			return c.substring(nameEQ.length, c.length);
		}
	}

	return null;
};

const eraseCookie = name => {
	document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

function images() {
	const nodes = document.querySelectorAll('img');
	for (const node of nodes) {
		node.className = 'img-fluid rounded';

		const url = node.dataset.original;
		if (url) {
			node.src = url;
		}
	}
}

function convertTitleAttributeToFigCaption() {
	for (const node of document.querySelectorAll('img[title]')) {
		const title = node.title;
		if (title) {
			const f = document.createElement('figure');
			f.className = 'figure';
			const fc = document.createElement('figcaption');
			fc.innerHTML = node.title;
			fc.className = 'figure-caption';
			node.removeAttribute('title');
			node.className = 'figure-img img-fluid rounded';

			node.parentNode.append(f);
			f.append(node);
			f.append(fc);
		}
	}
}

async function visited() {
	const visited = await getVisisted();
	for (const link of document.querySelectorAll('a:not(.btn)')) {
		const key = 'visited-' + link.pathname + link.search;
		if (
			link.host == window.location.host
            && visited.some(x => x.visited === key)
		) {
			const span = link.querySelector('span > span');

			if (span) {
				span.classList.remove('text-bg-primary');
				span.classList.add('text-bg-secondary');
			}
		}
	}
}

async function clearOldVisited() {
	const uId = getUserId();

	if (uId) {
		const response = await fetch(`http://pihole.lan:54321/api/feeds/read/${uId}`, {
			method: 'DELETE',
			mode: 'cors',
			headers: new Headers({
				Accept: 'application/json',
				'Content-Type': 'application/json',
			}),
		});

		return response.json();
	}
}

window.addEventListener(
	'popstate',
	() => {
		markAsRead();
		visited();
	},
	false,
);

window.addEventListener(
	'pageshow',
	() => {
		markAsRead();
		visited();
	},
	false,
);

const getPreferredTheme = () => window.matchMedia(prefersColorScheme).matches ? 'dark' : 'light';

const setTheme = theme => {
	document.documentElement.dataset.bsTheme = theme === 'auto' && window.matchMedia(prefersColorScheme).matches ? 'dark' : theme;
};

window.matchMedia(prefersColorScheme).addEventListener('change', () => {
	setTheme(getPreferredTheme());
});

function askForUserId() {
	const userId = getUserId();

	if (!userId) {
		const token = prompt('Add user id');

		if (token) {
			localStorage.setItem('token', token);
			const uId = getUserId();
			if (!uId) {
				setCookie('token', token);
			}
		}
	}
}

window.addEventListener('load', () => {
	setTheme(getPreferredTheme());
	askForUserId();
	addDeleteListener();
	addTableClass();
	convertTitleAttributeToFigCaption();
	images();
	clearOldVisited();
});
