use crate::read;
use rusqlite::Connection;
use std::path::PathBuf;
extern crate uuid;
use self::uuid::Uuid;
use chrono::Utc;
use std::fs::File;
use std::io::Write;
use uuid::fmt::Hyphenated;

pub struct QueryBag {
    pub id: Hyphenated,
    pub title: String,
    pub feed_type: read::FeedType,
    pub link: String,
}

pub fn export_opml(conn: &Connection, outdir: PathBuf) {
    let sql = "SELECT * FROM feed WHERE tags LIKE '%#blogroll%' ORDER BY lower(title)";
    let mut stmt = conn.prepare(sql).unwrap();

    let rows = stmt
        .query_map([], |row| {
            Ok(QueryBag {
                id: Uuid::parse_str(row.get::<usize, String>(0).unwrap().as_str())
                    .unwrap()
                    .hyphenated(),
                feed_type: read::FeedType::try_from(row.get::<usize, i32>(4).unwrap()).unwrap(),
                title: row.get::<usize, String>(1)?,
                link: row.get::<usize, String>(5)?,
            })
        })
        .unwrap();

    let now_utc = Utc::now();
    let mut opml: Vec<String> = vec![];
    opml.push(r#"<?xml version="1.0" encoding="UTF-8"?>"#.to_string());
    opml.push(r#"<opml version="1.0">"#.to_string());
    opml.push(format!(
        r#"<head>
        <title>OPML for all feeds in Superrune's blogroll</title>
        <dateCreated>{0}</dateCreated>
    </head>"#,
        now_utc.to_rfc3339()
    ));
    opml.push("<body>".to_string());
    for row in rows {
        let r = row.unwrap();
        let outline = format!(
            r#"<outline text="{2}" title="{2}" type="{1}" xmlUrl="{0}" htmlUrl="{0}"/>"#,
            escaper(r.link),
            r.feed_type,
            escaper(r.title)
        );
        opml.push(outline);
    }
    opml.push("</body>".to_string());
    opml.push("</opml>".to_string());

    let path = outdir;
    let mut wopml = File::create(path.join("superrune_blogroll.opml")).unwrap();
    wopml.write_all(opml.join("").as_bytes()).unwrap();
}

fn escaper(input: String) -> String {
    let i = input.replace(r#"""#, "&quot;");
    

    i.replace("&", "&amp;")
}
