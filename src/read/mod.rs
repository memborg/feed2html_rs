extern crate chrono;
extern crate reqwest;
extern crate rusqlite;
extern crate sxd_document;
extern crate sxd_xpath;
extern crate uuid;

use self::sxd_document::dom::Root;
use self::sxd_document::parser;
use self::sxd_xpath::nodeset::Node;
use self::sxd_xpath::{Context, Factory, Value};
use self::uuid::Uuid;
use crate::scraper;
use chrono::prelude::*;
use chrono::TimeDelta;
use reqwest::header::HeaderMap;
use rusqlite::{params, Connection};
use std::fmt;
use std::io::Write;
use uuid::fmt::Hyphenated;

const RETENTION: i64 = 90; //Number of days feed entries are saved. After that they are deleted

#[derive(Debug)]
enum UpdatePeriod {
    Hourly,
    Daily,
    Weekly,
    Monthly,
    Yearly,
}

#[derive(Debug)]
pub enum FeedType {
    None = 0,
    Rss = 1,
    Atom = 2,
    Rdf = 3,
}

impl TryFrom<i32> for FeedType {
    type Error = ();

    fn try_from(v: i32) -> Result<Self, Self::Error> {
        match v {
            x if x == FeedType::Rss as i32 => Ok(FeedType::Rss),
            x if x == FeedType::Atom as i32 => Ok(FeedType::Atom),
            x if x == FeedType::Rdf as i32 => Ok(FeedType::Rdf),
            x if x == FeedType::None as i32 => Ok(FeedType::None),
            _ => Err(()),
        }
    }
}

impl fmt::Display for FeedType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
pub enum CleanBuffer {
    No = 0,
    Yes = 1,
}

#[derive(Debug)]
pub enum Scrape {
    No = 0,
    Yes = 1,
}

impl TryFrom<i32> for CleanBuffer {
    type Error = ();

    fn try_from(v: i32) -> Result<Self, Self::Error> {
        match v {
            x if x == CleanBuffer::Yes as i32 => Ok(CleanBuffer::Yes),
            x if x == CleanBuffer::No as i32 => Ok(CleanBuffer::No),
            _ => Err(()),
        }
    }
}

impl TryFrom<i32> for Scrape {
    type Error = ();

    fn try_from(v: i32) -> Result<Self, Self::Error> {
        match v {
            x if x == Scrape::Yes as i32 => Ok(Scrape::Yes),
            x if x == Scrape::No as i32 => Ok(Scrape::No),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Default)]
struct Media {
    pub url: String,
    pub mime_type: String,
    pub length: String,
}

#[derive(Debug)]
pub struct QueryBag {
    pub id: Hyphenated,
    pub feed_type: FeedType,
    pub link: String,
    pub clean_buffer: CleanBuffer,
    pub updated: DateTime<Utc>,
    pub last_run: DateTime<Utc>,
    pub scrape: Scrape,
    pub etag: Result<String, rusqlite::Error>,
    pub last_modified: Result<String, rusqlite::Error>,
}

#[derive(Debug)]
struct ItemInsertBag {
    id: String,
    title: String,
    description: String,
    feed_id: String,
    guid: String,
    pub_date: DateTime<Utc>,
    link: String,
}

enum HasMedia {
    No = 0,
    Yes = 1,
}

#[derive(Debug)]
struct FeedBag {
    id: Hyphenated,
    body: String,
    updated: DateTime<Utc>,
    last_run: DateTime<Utc>,
    scrape: Scrape,
    etag: String,
    last_modified: String,
    retry_after: u64,
}

pub fn read_feeds(conn: &Connection) -> i32 {
    let sql = "SELECT * FROM feed WHERE nextRun < ? ORDER BY lower(title)";

    let mut feed_count = 0;

    let mut stmt = conn.prepare(sql).unwrap();
    let rows = stmt
        .query_map(params![Utc::now()], |row| {
            Ok(QueryBag {
                id: Uuid::parse_str(row.get::<usize, String>(0).unwrap().as_str())
                    .unwrap()
                    .hyphenated(),
                feed_type: FeedType::try_from(row.get::<usize, i32>(4).unwrap()).unwrap(),
                link: row.get::<usize, String>(5)?,
                clean_buffer: CleanBuffer::try_from(row.get::<usize, i32>(6).unwrap()).unwrap(),
                updated: row.get::<usize, DateTime<Utc>>(2).unwrap(),
                last_run: row.get::<usize, DateTime<Utc>>(3).unwrap(),
                scrape: Scrape::try_from(row.get::<usize, i32>(8).unwrap()).unwrap(),
                etag: row.get::<usize, String>(9),
                last_modified: row.get::<usize, String>(10),
            })
        })
        .unwrap();

    for row in rows {
        let entry_count = load_feed(conn, row.unwrap());

        if entry_count > 0 {
            feed_count += 1;
        }
    }

    feed_count
}

fn get_etag(headers: &reqwest::header::HeaderMap) -> String {
    if let Some(hv) = headers.get("ETag") {
        if let Ok(s) = hv.to_str() {
            return String::from(s);
        }
    }

    if let Some(hv) = headers.get("etag") {
        if let Ok(s) = hv.to_str() {
            return String::from(s);
        }
    }

    String::new()
}

fn get_retry_after(headers: &reqwest::header::HeaderMap) -> u64 {
    if let Some(hv) = headers.get("Retry-After") {
        if let Ok(s) = hv.to_str() {
            if let Ok(i) = s.parse::<u64>() {
                return i;
            }
        }
    }

    if let Some(hv) = headers.get("retry-after") {
        if let Ok(s) = hv.to_str() {
            if let Ok(i) = s.parse::<u64>() {
                return i;
            }
        }
    }

    0
}

fn get_last_modified(headers: &reqwest::header::HeaderMap) -> String {
    if let Some(hv) = headers.get("Last-Modified") {
        if let Ok(s) = hv.to_str() {
            return String::from(s);
        }
    }

    if let Some(hv) = headers.get("last-modified") {
        if let Ok(s) = hv.to_str() {
            return String::from(s);
        }
    }

    String::new()
}

fn _get_ratelimit_used(headers: &reqwest::header::HeaderMap) -> u64 {
    if let Some(hv) = headers.get("X-Ratelimit-Used") {
        if let Ok(s) = hv.to_str() {
            return s.parse::<u64>().unwrap();
        }
    }

    0
}

fn _get_ratelimit_remaining(headers: &reqwest::header::HeaderMap) -> u64 {
    if let Some(hv) = headers.get("X-Ratelimit-Remaining") {
        if let Ok(s) = hv.to_str() {
            if let Ok(f) = s.parse::<f64>() {
                return f as u64;
            }

            if let Ok(l) = s.parse::<u64>() {
                return l;
            }
        }
    }

    0
}

fn get_ratelimit_reset(headers: &reqwest::header::HeaderMap) -> u64 {
    if let Some(hv) = headers.get("X-Ratelimit-Reset") {
        if let Ok(s) = hv.to_str() {
            return s.parse::<u64>().unwrap();
        }
    }

    0
}

fn resp_handler(conn: &Connection, resp: reqwest::blocking::Response, feed_bag: QueryBag) -> i32 {
    let mut entry_count = 0;

    let status = resp.status();
    let headers = resp.headers().clone();

    let body = resp.text().unwrap();

    let etag = get_etag(&headers);
    let last_modified = get_last_modified(&headers);
    let retry_after = get_retry_after(&headers);
    let retry_after = retry_after + get_ratelimit_reset(&headers);

    if status.is_client_error() {
        println!("{}", mutate_link(&feed_bag.link));
        println!("{} Status: {}", feed_bag.id, status);
        println!("{:#?}", headers);
        println!("{}", body.clone());

        let nr = next_run(
            UpdatePeriod::Daily,
            8,
            feed_bag.updated,
            feed_bag.last_run,
            retry_after,
        );

        update_feed_wrap(
            conn,
            &feed_bag.id,
            feed_bag.updated,
            nr,
            0,
            etag.clone(),
            last_modified.clone(),
        );
    }

    if !status.is_success() {
        return 0; //Break out if the status code it not between 200-299
    }

    let body = body.trim_start_matches('\u{feff}').to_string();

    let feed_bag2 = FeedBag {
        id: feed_bag.id,
        body,
        updated: feed_bag.updated,
        last_run: feed_bag.last_run,
        scrape: feed_bag.scrape,
        etag,
        last_modified,
        retry_after,
    };
    match feed_bag.feed_type {
        FeedType::Rss => {
            entry_count = query_rss_response(conn, feed_bag2);
        }
        FeedType::Rdf => {
            entry_count = query_rdf_response(conn, feed_bag2);
        }
        FeedType::Atom => {
            entry_count = query_atom_response(conn, feed_bag2);
        }
        _ => {}
    }

    if let CleanBuffer::Yes = feed_bag.clean_buffer {
        clean_buffer(conn, &feed_bag.id);
    }

    entry_count
}

fn load_feed(conn: &Connection, feed_bag: QueryBag) -> i32 {
    let mut entry_count = 0;

    let mut headers = HeaderMap::new();
    headers.insert(reqwest::header::USER_AGENT, "curl/7.88.1".parse().unwrap());
    headers.insert(reqwest::header::ACCEPT, "*/*".parse().unwrap());
    headers.insert(
        reqwest::header::ACCEPT_LANGUAGE,
        "en-US,en;q=0.5".parse().unwrap(),
    );
    headers.insert(reqwest::header::CACHE_CONTROL, "max-age=0".parse().unwrap());
    headers.insert(reqwest::header::CONNECTION, "keep-alive".parse().unwrap());

    if let Ok(ref etag) = feed_bag.etag {
        headers.insert(reqwest::header::IF_NONE_MATCH, etag.parse().unwrap());
    }

    if let Ok(ref last_modified) = feed_bag.last_modified {
        headers.insert(
            reqwest::header::IF_MODIFIED_SINCE,
            last_modified.parse().unwrap(),
        );
    }

    if let Ok(uri) = reqwest::Url::parse(&mutate_link(&feed_bag.link)) {
        let client = reqwest::blocking::Client::new();
        let response = client.get(uri).headers(headers);

        match response.send() {
            Ok(resp) => {
                entry_count = resp_handler(conn, resp, feed_bag);
            }
            Err(e) => {
                println!("{} - {}", Utc::now(), e);
            } //Failed to contact server
        };
    }

    entry_count
}

fn update_entry_description(conn: &Connection, entry_id: &Hyphenated, desc: &str) {
    let sql = "UPDATE entry SET description = ?1 WHERE id = ?2";

    match conn.execute(sql, params![desc, entry_id.to_string()]) {
        Ok(_) => {}
        Err(e) => {
            println!("update_entry_description {} - {}", Utc::now(), e);
        }
    }
}

fn update_feed(
    conn: &Connection,
    feed_id: &Hyphenated,
    updated: DateTime<Utc>,
    next_run: DateTime<Utc>,
    etag: String,
    last_modified: String,
) {
    let sql =
        "UPDATE feed SET updated = ?1, nextRun = ?2, etag = ?3, last_modified = ?4 WHERE id = ?5";

    match conn.execute(
        sql,
        params![updated, next_run, etag, last_modified, feed_id.to_string()],
    ) {
        Ok(_) => {}
        Err(e) => {
            println!("update-feed {} - {}", Utc::now(), e);
        }
    }
}

fn clean_buffer(conn: &Connection, feed_id: &Hyphenated) {
    clean_media(conn, feed_id);

    let sql = format!("DELETE FROM entry WHERE updated < DATETIME('now', 'localtime', '0 month', '-{} day') AND feedid = ?1", RETENTION);

    match conn.execute(&sql, params![feed_id.to_string()]) {
        Ok(_) => {}
        Err(e) => {
            println!("clean_buffer {} - {}", Utc::now(), e);
        }
    }
}

fn clean_media(conn: &Connection, feed_id: &Hyphenated) {
    match conn.execute("DELETE FROM media WHERE entryid IN (SELECT id FROM entry WHERE updated < DATETIME('now', 'localtime', '0 month', '-90 day') AND feedid = ?1);",
    params![feed_id.to_string()]) {
        Ok(_) => { },
        Err(e) => {
            println!("clean_media {} - {}", Utc::now(), e);
        }
    }
}

fn get_xpath(path: &str) -> Option<sxd_xpath::XPath> {
    let factory = Factory::new();
    if let Ok(xpath) = factory.build(path) {
        return xpath;
    }

    Option::None
}

fn insert_item(conn: &Connection, bag: ItemInsertBag) -> rusqlite::Result<usize> {
    conn.execute("INSERT INTO entry(id, title, link, updated, description, feedid, guid) VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7)",
    params![bag.id, bag.title, bag.link, bag.pub_date, bag.description, bag.feed_id, bag.guid])
}

fn entry_exists(conn: &Connection, guid: &str) -> bool {
    let sql = "select id from entry where guid = ?;";

    let mut stmt = conn.prepare(sql).unwrap();
    if let Ok(res) = stmt.exists(params![guid]) {
        return res;
    }

    false
}

fn process_rss_node(
    conn: &Connection,
    feed_id: &Hyphenated,
    scrape: &Scrape,
    node: sxd_xpath::nodeset::Node,
    root_node: Root,
    has_media: HasMedia,
) -> bool {
    let id = Uuid::new_v4();
    let id = id.hyphenated();
    let enclosures = enclosures(node);
    let mut media = media(node);
    let link = link(node);

    if let Some(iti) = itunes_image(node, root_node) {
        media.push(iti);
    }

    let mut bag = ItemInsertBag {
        id: id.to_string(),
        title: title(node),
        guid: guid(node),
        pub_date: updated(node),
        link: link.clone(),
        description: description(node),
        feed_id: feed_id.to_string(),
    };

    //If guid.is_empty() use link as guid
    if bag.guid.is_empty() {
        bag.guid.clone_from(&bag.link);
    }

    // Old feed entries are not taken into consideration
    let lower_limit = Utc::now() - TimeDelta::try_days(RETENTION).unwrap();
    if lower_limit > bag.pub_date {
        return false;
    }

    if !entry_exists(conn, &bag.guid) && insert_item(conn, bag).is_ok() {
        if let HasMedia::Yes = has_media {
            insert_media(conn, feed_id, &id, enclosures);
            insert_media(conn, feed_id, &id, media);
        }

        if let Scrape::Yes = scrape {
            if let Some(article) = scraper::get_article(&link, &id) {
                update_entry_description(conn, &id, &article);
            }
        }

        return true;
    }

    false
}

fn update_feed_wrap(
    conn: &Connection,
    feed_id: &Hyphenated,
    feed_date: DateTime<Utc>,
    nr: DateTime<Utc>,
    entry_count: i32,
    etag: String,
    last_modified: String,
) {
    if entry_count > 0 {
        update_feed(conn, feed_id, Utc::now(), nr, etag, last_modified);
    } else {
        update_feed(conn, feed_id, feed_date, nr, etag, last_modified);
    }
}

fn query_rss_response(conn: &Connection, feed_bag: FeedBag) -> i32 {
    let mut entry_count = 0;

    match parser::parse(&feed_bag.body) {
        Ok(package) => {
            let document = package.as_document();

            if let Some(xpath) = get_xpath("/rss/channel/item") {
                let _update_period = update_period(document.root());
                let _update_frequency: u32 = update_frequency(document.root());

                let context = Context::new();

                if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, document.root()) {
                    for node in nodes {
                        if process_rss_node(
                            conn,
                            &feed_bag.id,
                            &feed_bag.scrape,
                            node,
                            document.root(),
                            HasMedia::Yes,
                        ) {
                            entry_count += 1;
                        }
                    }
                }

                let nr = next_run(
                    _update_period,
                    _update_frequency,
                    feed_bag.updated,
                    feed_bag.last_run,
                    feed_bag.retry_after,
                );

                update_feed_wrap(
                    conn,
                    &feed_bag.id,
                    feed_bag.updated,
                    nr,
                    entry_count,
                    feed_bag.etag,
                    feed_bag.last_modified,
                );
            }
        }
        Err(e) => {
            let filename = format!("{}.xml", feed_bag.id);
            let mut file_ref = std::fs::File::create(filename).expect("create failed");
            file_ref
                .write_all(feed_bag.body.as_bytes())
                .expect("write failed");
            println!("{} - {} Rss {:?}", Utc::now(), feed_bag.id, e);
        }
    }

    entry_count
}

fn query_rdf_response(conn: &Connection, feed_bag: FeedBag) -> i32 {
    let mut entry_count = 0;

    match parser::parse(&feed_bag.body) {
        Ok(package) => {
            let document = package.as_document();

            if let Some(xpath) = get_xpath("/rdf:RDF/item") {
                let _update_period = update_period(document.root());
                let _update_frequency: u32 = update_frequency(document.root());

                let mut context = Context::new();
                context.set_namespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
                if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, document.root()) {
                    for node in nodes {
                        if process_rss_node(
                            conn,
                            &feed_bag.id,
                            &feed_bag.scrape,
                            node,
                            document.root(),
                            HasMedia::No,
                        ) {
                            entry_count += 1;
                        }
                    }
                }

                let nr = next_run(
                    _update_period,
                    _update_frequency,
                    feed_bag.updated,
                    feed_bag.last_run,
                    feed_bag.retry_after,
                );

                update_feed_wrap(
                    conn,
                    &feed_bag.id,
                    feed_bag.updated,
                    nr,
                    entry_count,
                    feed_bag.etag,
                    feed_bag.last_modified,
                );
            }
        }
        Err(e) => {
            println!("{} - {} Rdf {:?}", Utc::now(), feed_bag.id, e);
        }
    }

    entry_count
}

fn query_atom_response(conn: &Connection, feed_bag: FeedBag) -> i32 {
    let mut entry_count = 0;

    match parser::parse(&feed_bag.body) {
        Ok(package) => {
            let document = package.as_document();

            if let Some(xpath) = get_xpath("/a:feed/a:entry") {
                let _update_period = update_period(document.root());
                let _update_frequency: u32 = update_frequency(document.root());

                let mut context = Context::new();
                context.set_namespace("a", "http://www.w3.org/2005/Atom");

                if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, document.root()) {
                    for node in nodes {
                        let id = Uuid::new_v4();
                        let id = id.hyphenated();
                        let mg_description = media_group_description(node);
                        let mg_media = media_group_thumbnail(node);
                        let mut media: Vec<Media> = vec![];

                        if let Some(yt_id) = yt_video_id(node) {
                            media.push(yt_id);
                        }

                        let mut bag = ItemInsertBag {
                            id: id.to_string(),
                            title: atom_title(node),
                            guid: atom_id(node),
                            pub_date: updated(node),
                            link: link(node),
                            description: atom_content(node),
                            feed_id: feed_bag.id.to_string(),
                        };

                        if bag.description.is_empty() {
                            bag.description.clone_from(&mg_description);
                        }

                        let lower_limit = Utc::now() - TimeDelta::try_days(RETENTION).unwrap();
                        if lower_limit < bag.pub_date && insert_item(conn, bag).is_ok() {
                            insert_media(conn, &feed_bag.id, &id, mg_media);
                            insert_media(conn, &feed_bag.id, &id, media);

                            entry_count += 1;
                        }
                    }
                }

                let nr = next_run(
                    _update_period,
                    _update_frequency,
                    feed_bag.updated,
                    feed_bag.last_run,
                    feed_bag.retry_after,
                );

                update_feed_wrap(
                    conn,
                    &feed_bag.id,
                    feed_bag.updated,
                    nr,
                    entry_count,
                    feed_bag.etag,
                    feed_bag.last_modified,
                );
            }
        }
        Err(e) => {
            println!("{} - {} ATOM {:?}", Utc::now(), feed_bag.id, e);
        }
    }

    entry_count
}

fn next_run(
    period: UpdatePeriod,
    frequence: u32,
    feed_date: DateTime<Utc>,
    last_run: DateTime<Utc>,
    retry_after: u64,
) -> DateTime<Utc> {
    let mut nr: DateTime<Utc> = last_run;
    if last_run < feed_date {
        nr = feed_date;
    }

    if retry_after > 0 {
        nr = Utc::now();
        return nr + TimeDelta::try_seconds(retry_after.try_into().unwrap()).unwrap();
    }

    loop {
        match period {
            UpdatePeriod::Yearly => {
                let freq = 365 / frequence;
                nr = Utc
                    .with_ymd_and_hms(nr.year(), nr.month(), nr.day(), 0, 0, 0)
                    .unwrap()
                    + TimeDelta::try_days(i64::from(freq)).unwrap();
            }
            UpdatePeriod::Weekly => {
                let freq = 7 / frequence;
                nr = Utc
                    .with_ymd_and_hms(nr.year(), nr.month(), nr.day(), 0, 0, 0)
                    .unwrap()
                    + TimeDelta::try_days(i64::from(freq)).unwrap();
            }
            UpdatePeriod::Hourly => {
                let freq = 60 / frequence;
                nr = Utc
                    .with_ymd_and_hms(nr.year(), nr.month(), nr.day(), nr.hour(), nr.minute(), 0)
                    .unwrap()
                    + TimeDelta::try_minutes(i64::from(freq)).unwrap();
            }
            _ => {
                //Daily is default
                let freq = 24 / frequence;
                nr = Utc
                    .with_ymd_and_hms(nr.year(), nr.month(), nr.day(), nr.hour(), 0, 0)
                    .unwrap()
                    + TimeDelta::try_hours(i64::from(freq)).unwrap();
            }
        }

        if nr >= feed_date {
            break;
        }
    }

    nr
}

fn insert_media(conn: &Connection, feed_id: &Hyphenated, entry_id: &Hyphenated, media: Vec<Media>) {
    for m in media {
        match conn.execute(
            "INSERT INTO media(url, len, medium, feedid, entryid) VALUES(?1, ?2, ?3, ?4, ?5)",
            params![
                m.url,
                m.length,
                m.mime_type,
                feed_id.to_string(),
                entry_id.to_string()
            ],
        ) {
            Ok(_) => {}
            Err(e) => {
                println!("insert_media {} - {}", Utc::now(), e);
            }
        }
    }
}

fn title(node: Node) -> String {
    let mut _title = String::new();
    if let Some(xpath) = get_xpath("title") {
        let context = Context::new();

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for t in nodes {
                _title = t.string_value();
            }
        }
    }

    _title
}

fn atom_title(node: Node) -> String {
    let mut _title = String::new();
    if let Some(xpath) = get_xpath("a:title") {
        let mut context = Context::new();
        context.set_namespace("a", "http://www.w3.org/2005/Atom");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for t in nodes {
                _title = t.string_value();
            }
        }
    }

    _title
}

fn guid(node: Node) -> String {
    let mut _guid = String::new();
    if let Some(xpath) = get_xpath("guid") {
        let context = Context::new();

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for t in nodes {
                _guid = t.string_value();
            }
        }
    }

    _guid
}

fn atom_id(node: Node) -> String {
    let mut _guid = String::new();
    if let Some(xpath) = get_xpath("a:id") {
        let mut context = Context::new();
        context.set_namespace("a", "http://www.w3.org/2005/Atom");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for n in nodes {
                _guid = n.string_value();
            }
        }
    }

    _guid
}

fn link(node: Node) -> String {
    let mut _found = false;
    let mut _link = String::new();
    let mut context = Context::new();
    context.set_namespace("feedburner", "http://rssnamespace.org/feedburner/ext/1.0");

    let factory_feedburner_orig_link = Factory::new();
    let xpath_feedburner_orig_link = factory_feedburner_orig_link
        .build("feedburner:origLink")
        .expect("could not compile feedburner_orig_link expath");
    let xpath_feedburner_orig_link =
        xpath_feedburner_orig_link.expect("No origLink expath was compiled");

    let orig_link_node = xpath_feedburner_orig_link
        .evaluate(&context, node)
        .expect("feedburner_orig_link evaluation failed");

    if let Value::Nodeset(orig_link_nodes) = orig_link_node {
        for ol in orig_link_nodes {
            _link = ol.string_value();
            _found = true;
        }
    }

    if !_found {
        _link = link_atom(node);
        _found = !_link.is_empty();
    }

    if !_found {
        let factory_link = Factory::new();
        let xpath_link = factory_link
            .build("link")
            .expect("could not compile link expath");
        let xpath_link = xpath_link.expect("No link expath was compiled");

        let link_node = xpath_link
            .evaluate(&context, node)
            .expect("_link expath evaluation failed");

        if let Value::Nodeset(link_nodes) = link_node {
            for l in link_nodes {
                _link = l.string_value();
            }
        }
    }

    _link
}

fn link_atom(node: Node) -> String {
    let mut _link = String::new();

    if let Some(xpath) = get_xpath("a:link") {
        let mut context = Context::new();
        context.set_namespace("a", "http://www.w3.org/2005/Atom");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for node in nodes {
                _link = atom_href_attr(node);
            }
        }
    }
    _link
}

fn description(node: Node) -> String {
    let mut _description = String::new();
    let mut _found = false;

    if let Some(xpath) = get_xpath("content:encoded") {
        let mut context = Context::new();
        context.set_namespace("content", "http://purl.org/rss/1.0/modules/content/");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for node in nodes {
                _description = node.string_value();
                _found = true;
            }
        }
    }

    if !_found {
        if let Some(xpath) = get_xpath("description") {
            let context = Context::new();
            if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
                for node in nodes {
                    _description = node.string_value();
                }
            }
        }
    }

    _description
}

fn atom_content(node: Node) -> String {
    let mut _content = String::new();
    let mut _summary = String::new();

    let mut context = Context::new();
    context.set_namespace("a", "http://www.w3.org/2005/Atom");

    if let Some(xpath) = get_xpath("a:summary") {
        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for node in nodes {
                _summary = node.string_value();
            }
        }
    }

    if let Some(xpath) = get_xpath("a:content") {
        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for node in nodes {
                _content = node.string_value();
            }
        }
    }

    if _summary.len() > _content.len() {
        _summary
    } else {
        _content
    }
}

fn media_group_description(node: Node) -> String {
    let mut _description = String::new();

    if let Some(xpath) = get_xpath("media:group/media:description") {
        let mut context = Context::new();
        context.set_namespace("media", "http://search.yahoo.com/mrss/");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for node in nodes {
                _description = node.string_value();
            }
        }
    }

    _description
}

fn media_group_thumbnail(node: Node) -> Vec<Media> {
    let mut _media: Vec<Media> = vec![];

    if let Some(xpath) = get_xpath("media:group/media:thumbnail") {
        let mut context = Context::new();
        context.set_namespace("media", "http://search.yahoo.com/mrss/");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for node in nodes {
                let _url = url_attr(node);

                if !_url.is_empty() {
                    _media.push(Media {
                        url: _url,
                        length: String::new(),
                        mime_type: String::from("image"),
                    });
                }
            }
        }
    }

    _media
}

fn enclosures(node: Node) -> Vec<Media> {
    let mut _enclosures: Vec<Media> = vec![];
    if let Some(xpath) = get_xpath("enclosure") {
        let context = Context::new();

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for node in nodes {
                _enclosures.push(Media {
                    url: url_attr(node),
                    length: length_attr(node),
                    mime_type: mimetype_attr(node),
                });
            }
        }
    }

    _enclosures
}

fn url_attr(node: Node) -> String {
    let factory = Factory::new();
    let xpath = factory.build("@url").expect("Could not compile XPath");
    let xpath = xpath.expect("No XPath @url was compiled");

    let context = Context::new();

    let value = xpath
        .evaluate(&context, node)
        .expect("XPath evaluation failed");

    value.string()
}

fn atom_href_attr(node: Node) -> String {
    let mut context = Context::new();
    context.set_namespace("a", "http://www.w3.org/2005/Atom");

    let factory = Factory::new();
    let xpath = factory
        .build("@href")
        .expect("could not compile link expath");
    let xpath = xpath.expect("No @href expath was compiled");

    let value = xpath
        .evaluate(&context, node)
        .expect("_link expath evaluation failed");

    value.string()
}

fn length_attr(node: Node) -> String {
    let factory = Factory::new();
    let xpath = factory.build("@length").expect("Could not compile XPath");
    let xpath = xpath.expect("No XPath @length was compiled");

    let context = Context::new();

    let value = xpath
        .evaluate(&context, node)
        .expect("XPath evaluation failed");

    value.string()
}

fn mimetype_attr(node: Node) -> String {
    let factory = Factory::new();
    let xpath = factory.build("@type").expect("Could not compile XPath");
    let xpath = xpath.expect("No XPath @type was compiled");

    let context = Context::new();

    let value = xpath
        .evaluate(&context, node)
        .expect("XPath evaluation failed");

    value.string()
}

fn medium_attr(node: Node) -> String {
    let factory = Factory::new();
    let xpath = factory.build("@medium").expect("Could not compile XPath");
    let xpath = xpath.expect("No XPath @medium was compiled");

    let context = Context::new();

    let value = xpath
        .evaluate(&context, node)
        .expect("XPath evaluation failed");

    value.string()
}

fn media(node: Node) -> Vec<Media> {
    let mut _media: Vec<Media> = vec![];
    if let Some(xpath) = get_xpath("media:content") {
        let mut context = Context::new();
        context.set_namespace("media", "http://search.yahoo.com/mrss/");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for node in nodes {
                let _url = url_attr(node);

                if !_url.is_empty() {
                    _media.push(Media {
                        url: _url,
                        length: length_attr(node),
                        mime_type: medium_attr(node),
                    });
                }
            }
        }
    }

    _media
}

fn yt_video_id(node: Node) -> Option<Media> {
    if let Some(xpath) = get_xpath("yt:videoId") {
        let mut context = Context::new();
        context.set_namespace("yt", "http://www.youtube.com/xml/schemas/2015");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            let mut media: Media = Media::default();

            for node in nodes {
                media = Media {
                    url: String::from(node.string_value().as_str().trim()),
                    length: String::new(),
                    mime_type: String::from("youtube"),
                };
            }

            if media.url.chars().count() > 0 {
                return Some(media);
            }
        }
    }

    None
}

fn href(node: Node) -> String {
    let factory = Factory::new();
    let xpath = factory.build("@href").expect("Could not compile XPath");
    let xpath = xpath.expect("No XPath @type was compiled");

    let context = Context::new();

    let value = xpath
        .evaluate(&context, node)
        .expect("XPath evaluation failed");

    value.string()
}

fn itunes_image(node: Node, root_node: Root) -> Option<Media> {
    if let Some(xpath) = get_xpath("itunes:image") {
        let mut context = Context::new();
        context.set_namespace("itunes", "http://www.itunes.com/dtds/podcast-1.0.dtd");

        // Does the entry has a image attached?
        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            if let Some(node) = nodes.into_iter().next() {
                return Some(Media {
                    url: href(node),
                    length: String::new(),
                    mime_type: String::from("image"),
                });
            } else {
                //If not use the feeds cover image
                return feed_itunes_image(root_node);
            }
        }
    }

    None
}

fn feed_itunes_image(node: Root) -> Option<Media> {
    if let Some(xpath) = get_xpath("/rss/channel/itunes:image") {
        let mut context = Context::new();
        context.set_namespace("itunes", "http://www.itunes.com/dtds/podcast-1.0.dtd");

        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            if let Some(node) = nodes.into_iter().next() {
                return Some(Media {
                    url: href(node),
                    length: String::new(),
                    mime_type: String::from("image"),
                });
            }
        }
    }

    None
}

fn update_period(node: Root) -> UpdatePeriod {
    let mut _update_period = UpdatePeriod::Daily;
    if let Some(xpath) = get_xpath("/rss/channel/sy:updatePeriod") {
        let mut context = Context::new();
        context.set_namespace("sy", "http://purl.org/rss/1.0/modules/syndication/");
        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for up in nodes {
                _update_period = match up.string_value().trim().to_lowercase().as_str() {
                    "hourly" => UpdatePeriod::Hourly,
                    "weekly" => UpdatePeriod::Weekly,
                    "monthly" => UpdatePeriod::Monthly,
                    "yearly" => UpdatePeriod::Yearly,
                    _ => UpdatePeriod::Daily,
                };
            }
        }
    }

    _update_period
}

fn update_frequency(node: Root) -> u32 {
    let mut _update_frequency: u32 = 8; //Combined with update_period default value, feed check is 8 times every day.

    if let Some(xpath) = get_xpath("/rss/channel/sy:updateFrequency") {
        let mut context = Context::new();
        context.set_namespace("sy", "http://purl.org/rss/1.0/modules/syndication/");
        if let Ok(Value::Nodeset(nodes)) = xpath.evaluate(&context, node) {
            for uf in nodes {
                _update_frequency = uf.string_value().trim().parse::<u32>().unwrap();
            }
        }
    }

    _update_frequency
}

fn updated(node: Node) -> chrono::DateTime<chrono::Utc> {
    let mut _dt = Utc::now();
    let mut _found = false;
    let mut _fallback_dt = String::new();
    let mut context = Context::new();
    context.set_namespace("a10", "http://www.w3.org/2005/Atom");
    context.set_namespace("dc", "http://purl.org/dc/elements/1.1/");

    let factory_dc_date = Factory::new();
    let xpath_dc_date = factory_dc_date
        .build("dc:date")
        .expect("could not compile dc_date expath");
    let xpath_dc_date = xpath_dc_date.expect("no dc:date expath was compiled");

    let dc_date_node = xpath_dc_date
        .evaluate(&context, node)
        .expect("dc_date expath evaluation failed");
    if let Value::Nodeset(dc_updates_nodes) = dc_date_node {
        for dc in dc_updates_nodes {
            let dc_date = DateTime::parse_from_rfc3339(&dc.string_value());
            if let Ok(v) = dc_date {
                _dt = v.with_timezone(&chrono::Utc);
                _found = true;
            }
        }
    }

    //This check for the date the entry is published. Mostly used in Atom and is similar to pubDat
    //in Rss
    if !_found {
        let factory_a10_published = Factory::new();
        let xpath_a10_published = factory_a10_published
            .build("a10:published")
            .expect("could not compile a10_published expath");
        let xpath_a10_published =
            xpath_a10_published.expect("No a10:published expath was compiled");

        let a10_published_node = xpath_a10_published
            .evaluate(&context, node)
            .expect("a10_published expath evaluation failed");
        if let Value::Nodeset(a10_updates_nodes) = a10_published_node {
            for a10 in a10_updates_nodes {
                let a10_published = &a10.string_value().parse::<DateTime<Utc>>();
                if let Ok(v) = *a10_published {
                    _dt = v;
                    _found = true;
                }
            }
        }
    }

    //Used by both Rss and Atom
    //The in Atom updated is used when an entry has been updated.
    if !_found {
        let factory_a10_updated = Factory::new();
        let xpath_a10_updated = factory_a10_updated
            .build("a10:updated")
            .expect("could not compile a10_updated expath");
        let xpath_a10_updated = xpath_a10_updated.expect("No a19:updated expath was compiled");

        let a10_updated_node = xpath_a10_updated
            .evaluate(&context, node)
            .expect("a10_updated expath evaluation failed");
        if let Value::Nodeset(a10_updates_nodes) = a10_updated_node {
            for a10 in a10_updates_nodes {
                let a10_updated = &a10.string_value().parse::<DateTime<Utc>>();

                if let Ok(v) = *a10_updated {
                    _dt = v;
                    _found = true;
                }
            }
        }
    }

    if !_found {
        let factory_pub_date = Factory::new();
        let xpath_pub_date = factory_pub_date
            .build("pubDate")
            .expect("could not compile pub_date expath");
        let xpath_pub_date = xpath_pub_date.expect("No pubDate expath was compiled");

        let pub_date_node = xpath_pub_date
            .evaluate(&context, node)
            .expect("pub_date expath evaluation failed");
        if let Value::Nodeset(pub_date_nodes) = pub_date_node {
            for pd in pub_date_nodes {
                let pub_date = DateTime::parse_from_rfc2822(&pd.string_value());
                match pub_date {
                    Ok(v) => {
                        _dt = v.with_timezone(&chrono::Utc);
                        _found = true;
                    }
                    Err(_) => {
                        _fallback_dt = pd.string_value();
                    }
                };
            }
        }
    }

    if !_fallback_dt.is_empty() {
        if !_found {
            let format = "%a, %d %b %Y %T %z";
            let replaced = _fallback_dt.replace('Z', "+0000").replace("-0000", "+0000");
            if let Ok(v) = DateTime::parse_from_str(replaced.as_str(), format) {
                _dt = v.with_timezone(&chrono::Utc);
                _found = true;
            }
        }

        if !_found {
            let format = "%d %b %Y";
            if let Ok(v) = NaiveDate::parse_from_str(_fallback_dt.as_str(), format) {
                _dt = Utc
                    .with_ymd_and_hms(v.year(), v.month(), v.day(), 0, 0, 0)
                    .unwrap();
                _found = true;
            }
        }
    }

    _dt
}

fn mutate_link(link: &str) -> String {
    String::from(link)
}
