extern crate chrono;
extern crate rusqlite;

pub use crate::config;
use crate::scraper;
use chrono::prelude::*;
use chrono::TimeDelta;
use rusqlite::{params, Connection};
use std::env;
use std::fs;
use std::fs::{DirBuilder, File};
use std::io::prelude::*;
use std::path::PathBuf;

#[derive(Debug)]
struct Media {
    url: String,
    mime_type: String,
}

#[derive(Debug)]
struct Feed {
    id: String,
    title: String,
    updated: Result<DateTime<Utc>, rusqlite::Error>,
    tags: String,
}

#[derive(Debug)]
struct Entry {
    id: String,
    title: String,
    link: String,
    updated: Result<DateTime<Utc>, rusqlite::Error>,
    description: String,
}

pub fn write_feeds(conn: &Connection) {
    let short = "%Y%m%d%H%M%S";
    let pretty = "%Y-%m-%d %H:%M:%S";
    let today_short = "%Y%m%d";
    let now = Utc::now();
    let today = Utc
        .with_ymd_and_hms(now.year(), now.month(), now.day(), 0, 0, 0)
        .unwrap();
    let yesterday = today - TimeDelta::try_days(1).unwrap();
    let tomorrow = today + TimeDelta::try_days(1).unwrap();
    let folder_name = Utc::now().format(today_short).to_string();
    let yesterday_folder_name = yesterday.format(today_short).to_string();
    let tomorrow_folder_name = tomorrow.format(today_short).to_string();
    let templates = get_templates();
    let html_folder = get_html();
    let work_folder = html_folder.join(folder_name.clone());

    if !work_folder.exists() {
        DirBuilder::new().create(work_folder.clone()).unwrap();
    }

    let mut ftemplatefile = File::open(templates.join("feed.html")).unwrap();
    let mut feed_template = String::new();
    ftemplatefile.read_to_string(&mut feed_template).unwrap();

    let mut ffeed_index = File::open(templates.join("index.html")).unwrap();
    let mut feed_index_template = String::new();
    ffeed_index
        .read_to_string(&mut feed_index_template)
        .unwrap();

    let mut fmain = File::open(templates.join("main.html")).unwrap();
    let mut main_template = String::new();
    fmain.read_to_string(&mut main_template).unwrap();

    let feed_sql =
        "SELECT id, title, updated, tags FROM feed WHERE updated >= ? ORDER BY lower(title)";

    let mut stmt = conn.prepare(feed_sql).unwrap();
    let rows = stmt
        .query_map(params![today], |row| {
            Ok(Feed {
                id: row.get::<usize, String>(0).unwrap(),
                title: row.get::<usize, String>(1).unwrap(),
                updated: row.get::<usize, DateTime<Utc>>(2),
                tags: row.get::<usize, String>(3).unwrap(),
            })
        })
        .unwrap();

    let mut main_index: Vec<String> = vec![];
    for row in rows {
        let feed = row.unwrap();

        let feed_id = feed.id;
        let title = feed.title;
        let mut updated = Utc::now();

        if let Ok(u) = feed.updated {
            updated = u;
        }

        let tags = feed.tags;
        let mut entry_count = 0;

        let mut feed_index: Vec<String> = vec![];
        let mut body: Vec<String> = vec![];

        let entry_sql =
            "SELECT * FROM entry WHERE updated >= ?1 AND feedid = ?2 ORDER BY updated DESC";

        let mut stmt2 = conn.prepare(entry_sql).unwrap();
        let entries = stmt2
            .query_map(params![today, feed_id], |row| {
                Ok(Entry {
                    id: row.get::<usize, String>(0).unwrap(),
                    title: row.get::<usize, String>(1).unwrap(),
                    link: row.get::<usize, String>(2).unwrap(),
                    updated: row.get::<usize, DateTime<Utc>>(3),
                    description: row.get::<usize, String>(4).unwrap(),
                })
            })
            .unwrap();

        let entry_count_sql =
            "SELECT count(id) FROM entry WHERE updated >= ?1 AND feedid = ?2 ORDER BY updated DESC";

        let mut count = 1;
        let mut stmt2 = conn.prepare(entry_count_sql).unwrap();
        if let Ok(c) = stmt2.query_row(params![today, feed_id], |r| r.get(0)) {
            count = c;
            entry_count = count;
        }

        for e in entries {
            let entry = e.unwrap();

            let entry_id = entry.id;
            let entry_title = entry.title;
            let entry_link = entry.link;
            let mut entry_updated = Utc::now();

            if let Ok(eu) = entry.updated {
                entry_updated = eu;
            }

            if entry_count == 0 {
                updated = entry_updated;
            }

            feed_index.push(get_feed_index_item(&entry_id, &entry_title, count));

            let description = scraper::scrape_description(entry.description, entry_link.clone());
            let entry_updated = entry_updated
                .with_timezone(&chrono::Local)
                .format(pretty)
                .to_string();

            let media = get_media(conn, &entry_id, &title, &entry_title);
            body.push(get_article(
                &feed_id,
                &entry_id,
                &entry_title,
                &entry_updated,
                &entry_link,
                &tags.replace('#', ""),
                &description,
                &media,
            ));

            if count > 1 {
                count -= 1;
            }
        }

        if entry_count > 0 {
            let feed_file_name = format!("{}.html", feed_id);

            let mut html = feed_template.replace("{title}", &title);
            html = html.replace("{feed_id}", &feed_id);
            html = html.replace("{index}", &feed_index.join(""));
            html = html.replace("{entries}", &body.join(""));
            html = html.replace("{subfolder_prefix}", &get_subfolder_prefix());

            let mut wfeed = File::create(work_folder.join(feed_file_name)).unwrap();
            wfeed.write_all(html.as_bytes()).unwrap();

            main_index.push(get_main_index_item(
                &feed_id,
                &updated.format(short).to_string(),
                &title,
                entry_count,
            ));
        }
    }

    let mut feed_index = feed_index_template.replace("{title}", &folder_name);
    feed_index = feed_index.replace("{yesterday}", &yesterday_folder_name);
    feed_index = feed_index.replace("{today}", &folder_name);
    feed_index = feed_index.replace("{tomorrow}", &tomorrow_folder_name);
    feed_index = feed_index.replace("{index}", &main_index.join(""));
    feed_index = feed_index.replace("{subfolder_prefix}", &get_subfolder_prefix());
    feed_index = feed_index.replace(
        "{updated}",
        Local::now().format(pretty).to_string().as_str(),
    );

    let mut wfindex = File::create(work_folder.join("index.html")).unwrap();
    wfindex.write_all(feed_index.as_bytes()).unwrap();

    let latest = format!("{}{}", &get_subfolder_prefix(), folder_name);
    main_template = main_template.replace("{latestnews}", latest.as_str());
    main_template = main_template.replace("{subfolder_prefix}", &get_subfolder_prefix());

    let mut wmain = File::create(html_folder.join("index.html")).unwrap();
    wmain.write_all(main_template.as_bytes()).unwrap();

    let mut faddfeedfile = File::open(templates.join("add-feed.html")).unwrap();
    let mut add_feed_template = String::new();
    faddfeedfile.read_to_string(&mut add_feed_template).unwrap();

    let latest = format!("{}{}", &get_subfolder_prefix(), folder_name);
    add_feed_template = add_feed_template.replace("{latestnews}", latest.as_str());
    add_feed_template = add_feed_template.replace("{subfolder_prefix}", &get_subfolder_prefix());

    let mut waddfeed = File::create(html_folder.join("add-feed.html")).unwrap();
    waddfeed.write_all(add_feed_template.as_bytes()).unwrap();

    clean_up();
}

fn get_main_index_item(feed_id: &str, updated: &str, feed_title: &str, entry_count: i32) -> String {
    let path = get_partials().join("main_index_item.html");

    let partial = get_file_content(path);
    let partial = partial.replace("{feed_id}", feed_id);
    let partial = partial.replace("{updated}", updated);
    let partial = partial.replace("{feed_title}", feed_title);
    let partial = partial.replace("{entry_count}", entry_count.to_string().as_str());

    partial.replace("{subfolder_prefix}", &get_subfolder_prefix())
}

fn youtube(yt_id: &str, entry_id: &str, feed_title: &str, entry_title: &str) -> String {
    let path = get_partials().join("youtube.html");
    let title = format!("{}: {}", feed_title, entry_title);

    let partial = get_file_content(path);

    let partial = partial.replace("{title}", &title);
    let partial = partial.replace("{entry_id}", entry_id);
    partial.replace("{yt_id}", yt_id)
}

fn video(url: &str) -> String {
    let path = get_partials().join("video.html");

    let partial = get_file_content(path);

    partial.replace("{url}", url)
}

fn audio(entry_id: &str, url: &str, medium: &str, feed_title: &str, entry_title: &str) -> String {
    let path = get_partials().join("audio.html");
    let title = format!("{}: {}", feed_title, entry_title);

    let partial = get_file_content(path);
    let partial = partial.replace("{url}", url);
    let partial = partial.replace("{title}", &title);
    let partial = partial.replace("{entry_id}", entry_id);

    partial.replace("{medium}", medium)
}

fn image(url: &str) -> String {
    let path = get_partials().join("image.html");

    let partial = get_file_content(path);

    partial.replace("{url}", url)
}

fn get_feed_index_item(entry_id: &str, entry_title: &str, entry_count: i32) -> String {
    let path = get_partials().join("feed_index_item.html");

    let partial = get_file_content(path);
    let partial = partial.replace("{entry_id}", entry_id);
    let partial = partial.replace("{entry_title}", entry_title);
    let partial = partial.replace("{entry_count}", entry_count.to_string().as_str());

    partial.replace("{subfolder_prefix}", &get_subfolder_prefix())
}

fn get_article(
    feed_id: &str,
    entry_id: &str,
    entry_title: &str,
    entry_updated: &str,
    entry_link: &str,
    tags: &str,
    description: &str,
    media: &str,
) -> String {
    let path = get_partials().join("article.html");

    let partial = get_file_content(path);
    let partial = partial.replace("{feed_id}", feed_id);
    let partial = partial.replace("{entry_id}", entry_id);
    let partial = partial.replace("{entry_title}", entry_title);
    let partial = partial.replace("{entry_updated}", entry_updated);
    let partial = partial.replace("{entry_link}", entry_link);
    let partial = partial.replace("{tags}", tags);
    let partial = partial.replace("{description}", description);
    let partial = partial.replace("{subfolder_prefix}", &get_subfolder_prefix());

    partial.replace("{media}", media)
}

fn get_media(conn: &Connection, entry_id: &str, feed_title: &str, entry_title: &str) -> String {
    let mut m: Vec<String> = vec![];
    let sql_media =
        "SELECT url, medium FROM media WHERE entryid = ?1 AND url NOT LIKE '%gravatar%'";
    let mut stmt3 = conn.prepare(sql_media).unwrap();
    let medias = stmt3
        .query_map([&entry_id], |row| {
            Ok(Media {
                url: row.get::<usize, String>(0).unwrap(),
                mime_type: row.get::<usize, String>(1).unwrap(),
            })
        })
        .unwrap();

    for row in medias {
        let media = row.unwrap();
        let url = media.url;
        let medium = media.mime_type;

        match medium.as_str() {
            "youtube" => m.push(youtube(&url, entry_id, feed_title, entry_title)),
            "video/mov" => m.push(video(&url)),
            "video/mp4" => m.push(video(&url)),
            _ => {}
        }

        if medium.as_str().contains("image") {
            m.push(image(&url));
        }

        if medium.as_str().contains("audio") {
            m.push(audio(entry_id, &url, &medium, feed_title, entry_title));
        }
    }

    m.join("")
}

fn clean_up() {
    let yesterday = Utc::now() - TimeDelta::try_days(2).unwrap();
    let folder_name = yesterday.format("%Y%m%d").to_string();
    let html_folder = get_html();
    let work_folder = html_folder.join(folder_name);

    if work_folder.exists() {
        fs::remove_dir_all(work_folder).unwrap();
    }
}

fn get_root_folder() -> PathBuf {
    let exe = env::current_exe().unwrap();
    exe.parent().unwrap().to_path_buf()
}

fn get_config() -> config::Config {
    let config_path = get_root_folder().join("config.toml");

    let mut fconfig = File::open(config_path).unwrap();
    let mut config_str = String::new();
    fconfig.read_to_string(&mut config_str).unwrap();

    toml::from_str(&config_str).unwrap()
}

fn get_subfolder_prefix() -> String {
    let config = get_config();

    if let Some(sf) = config.subfolder {
        return sf.subfolder_prefix;
    }

    "/".to_string()
}

fn get_templates() -> PathBuf {
    get_root_folder().join("template")
}

fn get_html() -> PathBuf {
    get_root_folder().join("html")
}

fn get_partials() -> PathBuf {
    get_templates().join("partials")
}

fn get_file_content(path: PathBuf) -> String {
    let mut partial_file = File::open(path).unwrap();
    let mut partial = String::new();
    partial_file.read_to_string(&mut partial).unwrap();

    partial
}
