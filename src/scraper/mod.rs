extern crate ammonia;
extern crate reqwest;
extern crate scraper;
extern crate uuid;

use self::scraper::{Html, Selector};
use reqwest::header::HeaderMap;

pub fn get_article(link: &str, _id: &uuid::fmt::Hyphenated) -> Option<String> {
    if let Ok(uri) = reqwest::Url::parse(link) {
        let mut headers = HeaderMap::new();
        headers.insert(reqwest::header::USER_AGENT, "curl/7.88.1".parse().unwrap());
        headers.insert(reqwest::header::ACCEPT, "*/*".parse().unwrap());
        headers.insert(
            reqwest::header::ACCEPT_LANGUAGE,
            "en-US,en;q=0.5".parse().unwrap(),
        );
        headers.insert(reqwest::header::CACHE_CONTROL, "max-age=0".parse().unwrap());
        headers.insert(reqwest::header::CONNECTION, "keep-alive".parse().unwrap());

        let client = reqwest::blocking::Client::new();
        let response = client.get(uri).headers(headers);

        if let Ok(resp) = response.send() {
            if let Ok(body) = resp.text() {
                let frag = Html::parse_document(&body);

                if let Ok(selector) = Selector::parse("article") {
                    if let Some(fragment) = frag.select(&selector).next() {
                        let article = fragment.inner_html();

                        return Some(article);
                    }
                }

                if let Ok(selector) = Selector::parse("div > article") {
                    if let Some(fragment) = frag.select(&selector).next() {
                        let article = fragment.inner_html();

                        return Some(article);
                    }
                }

                if let Ok(selector) = Selector::parse("div > section") {
                    if let Some(fragment) = frag.select(&selector).next() {
                        let article = fragment.inner_html();

                        return Some(article);
                    }
                }
            }
        }
    }

    None
}

pub fn scrape_description(description: String, link: String) -> String {
    let mut desc = description;

    if let Ok(uri) = reqwest::Url::parse(&link) {
        let mut base_uri = String::new();
        base_uri.push_str(uri.scheme());
        base_uri.push_str("://");
        base_uri.push_str(uri.domain().unwrap());
        desc = fix_base_paths(&desc, &base_uri);
    }

    let frag = Html::parse_document(&desc);
    let selector = Selector::parse("body").unwrap();

    if let Some(fragment) = frag.select(&selector).next() {
        desc = fragment.inner_html();
    }

    desc = ammonia::Builder::default()
        .add_tags(&["iframe", "video", "source", "audio"])
        .add_tag_attributes(
            "iframe",
            &[
                "name",
                "src",
                "height",
                "width",
                "allow",
                "allowfullscreen",
                "frameborder",
            ],
        )
        .add_tag_attributes(
            "video",
            &[
                "poster", "controls", "height", "width", "preload", "src", "loop",
            ],
        )
        .add_tag_attributes(
            "audio",
            &[
                "poster", "controls", "height", "width", "preload", "src", "loop",
            ],
        )
        .add_tag_attributes(
            "source",
            &["src", "type", "srcset", "sizes", "media", "height", "width"],
        )
        .add_tag_attributes("img", &["data-original"])
        .rm_tags(&["h1", "header", "footer", "noscript", "nav", "center"])
        .clean(desc.as_str())
        .to_string();

    desc = fix_img(&desc);
    desc = fix_paths(&desc);
    desc
    // linkify(&desc)
}

fn fix_img(body: &str) -> String {
    let mut desc = String::from(body);

    desc = desc.as_str().replace("&amp;amp;lt;", "<");
    desc = desc.as_str().replace("&amp;amp;gt;", ">");

    desc
}

fn fix_paths(body: &str) -> String {
    let mut desc = String::from(body);
    desc = desc.as_str().replace("src=\"../..", "src=\"");

    desc
}

fn fix_base_paths(body: &str, base: &str) -> String {
    //Because the world is not perfect we need to insert base-path of the feeds website to ensure
    //everything gets loaded.  And sometimes the base-path is still not enough

    let mut desc = String::from(body);
    desc = desc.as_str().replace("src=\"//", "src=\"https://");
    desc = desc
        .as_str()
        .replace("src=\"/", format!("src=\"{}/", base).as_str());
    desc = desc
        .as_str()
        .replace("href=\"/", format!("href=\"{}/", base).as_str());

    desc
}

fn linkify(body: &str) -> String {
    let mut desc = process_body_links(r#" https://"#, body);
    desc = process_body_links(r#" http://"#, &desc);

    desc
}

fn process_body_links(selector: &str, body: &str) -> String {
    let indices: Vec<usize> = body.match_indices(selector).map(|(i, _)| i).collect();

    if indices.is_empty() {
        return String::from(body);
    }

    let mut desc = String::from(body);

    let links: Vec<String> = process_links(indices, body);

    for link in links {
        desc = desc.replace(
            &link,
            format!(r#"<a href="{l}">{l}</a>"#, l = link).as_str(),
        )
    }

    desc
}

fn process_links(indices: Vec<usize>, body: &str) -> Vec<String> {
    let illegal: String = String::from(r#""<>^`{|}"#);
    let mut links: Vec<String> = vec![];
    for i in indices {
        let mut chrs = body.char_indices();
        let mut link = String::new();
        loop {
            let start = i + 1;

            if let Some(ch) = chrs.next() {
                let indice = ch.0;
                if indice >= start {
                    let c = ch.1;

                    if illegal.contains(c) {
                        if !links.contains(&link) {
                            links.push(link);
                        }
                        break;
                    }

                    if c.is_whitespace() {
                        if !links.contains(&link) {
                            links.push(link);
                        }
                        break;
                    }

                    link.push(c);
                }
            } else {
                if !links.contains(&link) {
                    links.push(link);
                }
                break;
            }
        }
    }

    links
}
