extern crate chrono;
extern crate rusqlite;

use chrono::Utc;

#[macro_use]
extern crate serde_derive;

use rusqlite::Connection;
use std::path::Path;

pub mod config;
pub mod export;
pub mod read;
pub mod scraper;
pub mod upload;
pub mod write;

#[derive(Debug)]
pub struct AppArgs {
    pub output: Option<std::path::PathBuf>,
    pub action: Option<String>,
}

fn main() {
    let exe = std::env::current_exe().unwrap();
    let root = exe.parent().unwrap();
    let root = root.join("data");
    let root = root.join("data.sqlite");

    println!("Starting {}", Utc::now());

    let data_path = Path::new(&root);
    match Connection::open(data_path) {
        Ok(conn) => {
            pre_query(&conn);
            let mut pargs = pico_args::Arguments::from_env();
            let args = AppArgs {
                output: pargs
                    .opt_value_from_os_str("--outdir", parse_path)
                    .expect("Output dir for opml file is required"),
                action: pargs.opt_value_from_str("--action").unwrap(),
            };
            if args.action == Some(String::from("export")) {
                export::export_opml(&conn, args.output.unwrap());
            } else {
                process(&conn);
            }
            conn.close().unwrap();
        }
        Err(e) => {
            println!("{} - {}", Utc::now(), e);
        }
    }

    println!("DONE {}", Utc::now());
}

fn process(conn: &Connection) {
    println!("READING {}", Utc::now());
    let feed_count = read::read_feeds(conn);

    println!("PROCESSED {} FEED ", feed_count);

    println!("WRITING {}", Utc::now());
    write::write_feeds(conn);

    vacuum(conn);

    if feed_count > 0 {
        println!("UPLOADING {}", Utc::now());
        upload::upload_feeds();
    }
}

fn pre_query(conn: &Connection) {
    let sql = r#"
        pragma journal_mode = WAL;
        pragma synchronous = normal;
        pragma temp_store = memory;
        pragma mmap_size = 2147483648;
        "#;

    match conn.execute_batch(sql) {
        Ok(_) => {}
        Err(e) => {
            println!("pre_query {} - {}", Utc::now(), e);
        }
    }
}

fn vacuum(conn: &Connection) {
    let sql = r#"
        pragma vacuum;
        pragma optimize;
        vacuum;
        "#;

    match conn.execute_batch(sql) {
        Ok(_) => {}
        Err(e) => {
            println!("vacuum {} - {}", Utc::now(), e);
        }
    }
}

fn parse_path(s: &std::ffi::OsStr) -> Result<std::path::PathBuf, &'static str> {
    Ok(s.into())
}
