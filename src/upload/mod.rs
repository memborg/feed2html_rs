extern crate rustydav;
extern crate toml;

use rustydav::client;

pub use crate::config;
use chrono::TimeDelta;
use chrono::Utc;
use std::env;
use std::fs::{self, File};
use std::io::BufReader;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;
use std::process::Command;

pub fn upload_feeds() {
    let folder_name = Utc::now().format("%Y%m%d").to_string();
    let root = get_root_folder();
    let html_folder = root.join("html");
    let work_folder = html_folder.join(folder_name);

    let config = get_config();

    if config.subfolder.is_none() {
        if let Some(n) = config.netlify {
            netlify(html_folder, n);
        } else {
            upload_webdav(config, &html_folder, &work_folder);
        }
    }
}

fn netlify(html_folder: PathBuf, netlify: config::Netlify) {
    let mut html_folder = html_folder.into_os_string().into_string().unwrap();
    let mut program = "netlify";
    if cfg!(target_os = "windows") {
        program = "netlify.cmd";
        html_folder = html_folder.replace("\\\\?\\", "");
    }
    let _ = Command::new(program)
        .arg("deploy")
        .arg("--prod")
        .arg("--dir")
        .arg(&html_folder)
        .arg("--site")
        .arg(&netlify.site)
        .status();
}

fn upload_webdav(config: config::Config, html_folder: &Path, work_folder: &PathBuf) {
    let folder_name = Utc::now().format("%Y%m%d").to_string();
    let old_folder_name = (Utc::now() - TimeDelta::try_days(10).unwrap())
        .format("%Y%m%d")
        .to_string();

    let webdav = client::Client::init(&config.ftp.user, &config.ftp.password);

    let server_path = format!(
        "{0}/{1}/{2}",
        &config.ftp.host, &config.ftp.root, folder_name
    );

    //Make folder
    let result = webdav.mkcol(&server_path);

    if let Err(err) = result {
        println!("Mkdir {0} failed: {1}", &server_path, err);
    }

    //Upload files
    let path = Path::new(work_folder);

    for entry in fs::read_dir(path).expect("Unable to list files") {
        let entry = entry.expect("Unable to read entry");

        let path = entry.path();

        if let Some(filename) = path.file_name() {
            let f = File::open(&path).unwrap();
            let mut reader = BufReader::new(f);
            let mut buffer = Vec::new();
            reader.read_to_end(&mut buffer).unwrap();

            let file_path = format!("{0}/{1}", &server_path, &filename.to_str().unwrap());

            let result = webdav.put(buffer, &file_path);

            if let Err(err) = result {
                println!("Put {0} failed: {1}", &file_path, err);
            }
        }
    }

    //Upload index.html
    let server_path = format!("{0}/{1}", &config.ftp.host, &config.ftp.root);

    let file_path = format!("{0}/{1}", &server_path, "index.html");

    let f = File::open(html_folder.join("index.html")).unwrap();
    let mut reader = BufReader::new(f);
    let mut buffer = Vec::new();
    reader.read_to_end(&mut buffer).unwrap();

    let result = webdav.put(buffer, &file_path);

    if let Err(err) = result {
        println!("Put {0} failed: {1}", &file_path, err);
    }

    //Delete old folder
    let server_path = format!(
        "{0}/{1}/{2}",
        &config.ftp.host, &config.ftp.root, &old_folder_name
    );

    let result = webdav.delete(&server_path);
    if let Err(err) = result {
        println!("Delete {0} failed: {1}", &server_path, err);
    }
}

fn get_root_folder() -> PathBuf {
    let exe = env::current_exe().unwrap();
    exe.parent().unwrap().to_path_buf()
}

fn get_config() -> config::Config {
    let config_path = get_root_folder().join("config.toml");

    let mut fconfig = File::open(config_path).unwrap();
    let mut config_str = String::new();
    fconfig.read_to_string(&mut config_str).unwrap();

    toml::from_str(&config_str).unwrap()
}
