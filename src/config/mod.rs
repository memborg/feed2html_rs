#[derive(Deserialize)]
pub struct Config {
    pub ftp: FtpConfig,
    pub netlify: Option<Netlify>,
    pub subfolder: Option<Subfolder>,
}

#[derive(Deserialize)]
pub struct FtpConfig {
    pub root: String,
    pub host: String,
    pub user: String,
    pub password: String,
}

#[derive(Deserialize)]
pub struct Netlify {
    pub site: String,
}

#[derive(Deserialize)]
pub struct Subfolder {
    pub subfolder_prefix: String,
}
